package wordgenserver

import (
	"bitbucket.org/jrhansen/wordgen"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"sort"
	"strings"
)

var wordGenerator wordgen.WordGenerator

func init() {
	wordGenerator = wordgen.New("words")
	r := mux.NewRouter()
	letters := "{letters:[a-zA-Z]+}"
	r.HandleFunc("/html/"+letters, htmlHandler)
	r.HandleFunc("/json/"+letters, jsonHandler)
	r.HandleFunc("/text/"+letters, textHandler)
	r.HandleFunc("/xml/"+letters, xmlHandler)
	http.Handle("/", r)
}

type wordSorter struct {
	words []string
	by    func(w1, w2 string) bool // Closure used in the Less method.
}

func (s *wordSorter) Len() int {
	return len(s.words)
}

func (s *wordSorter) Swap(i, j int) {
	s.words[i], s.words[j] = s.words[j], s.words[i]
}

func (s *wordSorter) Less(i, j int) bool {
	return s.by(s.words[i], s.words[j])
}

func sortByLength(words []string, asc bool) {
	byLength := func(w1, w2 string) bool {
		return len(w1) < len(w2)
	}

  sortBy(words, byLength, asc)
}

func sortByAlpha(words []string, asc bool) {
	byAlpha := func(w1, w2 string) bool {
		return w1 < w2
	}

  sortBy(words, byAlpha, asc)
}

func sortBy(words []string, by func(string, string) bool, asc bool) {
	var sorter sort.Interface = &wordSorter{words, by}

  if !asc {
    sorter = sort.Reverse(sorter)
  }

  sort.Sort(sorter)
}

func findWords(r *http.Request) []string {
	vars := mux.Vars(r)
	letters := strings.ToLower(vars["letters"])
	wordChan := make(chan string)
	wordGenerator.Find(letters, wordChan)

	var words []string

	for word := range wordChan {
		words = append(words, word)
	}

	values := r.URL.Query()

	sortBy := values["sortby"]
	if len(sortBy) == 1 {
		switch sortBy[0] {
		case "lengthasc":
			sortByLength(words, true)
		case "lengthdesc":
			sortByLength(words, false)
    case "alphadesc":
      sortByAlpha(words, false)
		}
	}

	return words
}

func htmlHandler(w http.ResponseWriter, r *http.Request) {
	writeWordsHtml(w, findWords(r))
}

func jsonHandler(w http.ResponseWriter, r *http.Request) {
	writeWordsJson(w, findWords(r))
}

func textHandler(w http.ResponseWriter, r *http.Request) {
	writeWordsText(w, findWords(r))
}

func xmlHandler(w http.ResponseWriter, r *http.Request) {
	writeWordsXml(w, findWords(r))
}

func writeWordsHtml(w http.ResponseWriter, words []string) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprint(w, "<html><body><table>")
	fmt.Fprint(w, "<tr><td><h1>Results</h1></td></tr>")

	for _, word := range words {
		fmt.Fprintf(w, "<tr><td>%s</td></tr>", word)
	}
	fmt.Fprint(w, "</table></body></html>")
}

func writeWordsJson(w http.ResponseWriter, words []string) {
	w.Header().Set("Content-Type", "text/json; charset=utf-8")
	fmt.Fprint(w, "{\"words\": [")
	for i, word := range words {
		if i > 0 {
			fmt.Fprint(w, ",")
		}
		fmt.Fprintf(w, "\"%s\"", word)
	}
	fmt.Fprint(w, "]}")
}

func writeWordsText(w http.ResponseWriter, words []string) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	for _, word := range words {
		fmt.Fprintln(w, word)
	}
}

func writeWordsXml(w http.ResponseWriter, words []string) {
	w.Header().Set("Content-Type", "text/xml; charset=utf-8")
	fmt.Fprint(w, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
	fmt.Fprint(w, "<ArrayOfString>")
	for _, word := range words {
		fmt.Fprintf(w, "<string>%s</string>", word)
	}
	fmt.Fprint(w, "</ArrayOfString>")
}
